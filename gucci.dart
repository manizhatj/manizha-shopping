import 'store.dart';
import 'product.dart';

class Gucci extends Store {
  Gucci() : super('Gucci') {
    Product gucciBag = Product('bag', 10000);
    addProductToStore(gucciBag);
    Product gucciShoes = Product('shoes', 2000);
    addProductToStore(gucciShoes);
    Product gucciDress = Product('dress', 20000);
    addProductToStore(gucciDress);
  }
}
