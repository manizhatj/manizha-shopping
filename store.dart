import 'product.dart';
// A store has 3 products

class Store {
  String name;
  List<Product> _products;

  List<Product> get products => List.unmodifiable(_products);

  Store(String name) {
    this.name = name;
    this._products = List();
  }

  Store.withProducts(String name, List<Product> products) {
    this.name = name;
    this._products = products;
  }

  void addProductToStore(Product product) {
    _products.add(product);
  }

  bool productPresent(String productName) {
    for (Product product in _products) {
      if (product.name == productName) {
        return true;
      }
    }
    return false;
  }

  Product getProductByName(String productName) {
    for (Product product in _products) {
      if (product.name == productName) {
        return product;
      }
    }
    return null;
  }

  // double getProductPrice(Product product){
  //   for (Product product in _products){
  //     return product.price;
  //   }
  // }

  @override
  String toString() {
    return '$name ($runtimeType), $_products';
  }
}
