import 'dart:io';

import 'customer.dart';
import 'store.dart';
import 'gucci.dart';
import 'ikea.dart';
import 'chocolate_shop.dart';
import 'cart.dart';
import 'product.dart';

class Mall {
  Customer _currentCustomer;

  List<Store> stores;
  List<Customer> customers;

  Mall() {
    this.stores = List();
    this.customers = List();

// create 3 stores
    Store gucci =
        Gucci(); // what is the difference between Store gucci = Gucci(); and Gucci gucci = Gucci();?
    this.stores.add(gucci);
    Store ikea = Ikea();
    this.stores.add(ikea);
    Store chocolateShop = ChocolateShop();
    this.stores.add(chocolateShop);

// create 2 existing customers
    Customer lola = Customer('Lola', 20000, Cart());
    customers.add(lola);
    Customer bob = Customer('Bob', 200, Cart());
    customers.add(bob);
  }

  // void addStoreToMall(Store store) {
  //   stores.add(store);
  // }

  List<Customer> addCustomerToMall(Customer customer) {
    customers.add(customer);
    return customers;
  }

  void selectCustomerByName(String customerName) {
    _currentCustomer = getCustomer(customerName);
    if (_currentCustomer == null) {
      //The customer was not found, ask the customer for ID or kick him/her out
      _currentCustomer = signUpCustomer(customerName);
    }
  }

  Customer getCustomer(String customerName) {
    Customer foundCustomer;
    customerName = customerName.toLowerCase();
    for (var customer in customers) {
      if (customer.name.toLowerCase() == customerName) {
        foundCustomer = customer;
        return foundCustomer;
      }
    }
    return null;
  }

  Customer signUpCustomer(String customerName) {
    print('You are not yet a member. We will now help you register');
    stdout.writeln('Enter your balance');
    double newBalance = double.parse(stdin.readLineSync());

    Customer newCustomer = Customer(customerName, newBalance, Cart());
    addCustomerToMall(newCustomer);
    return newCustomer;
  }

  Store selectStoreByName(String storeName) {
    storeName = storeName.toLowerCase();
    //Store foundStore;
    for (var store in stores) {
      if (store.name.toLowerCase() == storeName) {
        //foundStore = store;
        return store;
      }
    }
    return null;
  }

  @override
  String toString() {
    return 'Mall: $stores - $customers';
  }

  Customer get getCurrentCustomer => _currentCustomer;

  void addProductToCart(Product product, int amount) {
    for (int i = 0; i < amount; i++) {
      _currentCustomer.addProductToCart(product);
    }
  }

  String receipt() {
    return _currentCustomer.receipt();
  }

  double calculateTotalPrice() {
    return _currentCustomer.calculateTotalPrice();
  }

  double updateBalance() {
    return _currentCustomer.updateBalance();
  }
}
