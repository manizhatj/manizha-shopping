Mall
- is the program that contains all the classes and is the space where the program runs.
- has Stores
- has Customers
Start shopping mall

add 3 stores
add 3 products per store
Start shopping

-> Ask for customer name
-> Input: customer name //add balance
-> accept customer name
-> show list of stores
-> Ask which store they want to shop in
-> Input: store name
-> Show selected store name and its products
-> Ask which product they want to purchase
-> Input: product name
-> Ask how many items
-> Input: number of items
-> Check if customer has enough balance to add items to shopping cart
    -> If yes, add to cart. Ask checkout or keep shopping.
        -> if keep shopping: return to store, ask which product?
        -> if checkout: go to checkout, calculate new balance, show summary.
            -> Ask: what do you want to do? Go to mall or leave mall.
                if go to mall, start over with which store...?
                if leave mall, quit program.
    -> if no, display: You don't have enough balance (and show balance).
        -> Ask what you want to do: go back to store, back to mall, leave mall.
            -> if back to store, ask to choose product.
            -> If back to mall, ask choose store.
            -> if leave mall, quit.



Store
- has Products

Product
- has a name
- has a price


Customer
- has a name
- has a balance
- has a Cart
-> can buy one or more Products (go to store, select product, put on shopping cart, buy more or checkout. At checkout, you see balance. Can't buy additional product if balance will go to below zero after.)

Cart
- one per customer
- can hold multiple products (a customer can purchase multiple products) -> method: add product
- checkout: shows list of products purchased and their prices, and return sum of prices of all selected products





