import 'store.dart';
import 'product.dart';

class ChocolateShop extends Store {
  ChocolateShop() : super('Chocolate Shop') {
    addProductToStore(Product('dark', 3));
    addProductToStore(Product('milk', 2));
    addProductToStore(Product('white', 2.5));
  }
}
