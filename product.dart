class Product {
  String name;
  double price;

  Product(String name, double price) {
    this.name = name;
    this.price = price;
  }

  @override
  String toString() {
    return '$name, $price';
  }
}
