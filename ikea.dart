import 'store.dart';
import 'product.dart';

class Ikea extends Store {
  Ikea() : super('Ikea') {
    addProductToStore(Product('chair', 30));
    addProductToStore(Product('sofa', 300));
    addProductToStore(Product('pan', 10));
  }
}
