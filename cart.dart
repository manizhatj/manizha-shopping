import 'product.dart';
// a cart can contain multiple products
// can add product to cart

class Cart {
  List<Product> _products = List();

  void addProductToCart(Product product) {
    _products.add(product);
  }

  double calculateTotalPrice() {
    double sum = 0;
    this._products.forEach((product) {
      sum += product.price;
    });
    return sum;
  }

  Map getGroupedProducts() {
    Map result = {};

    this._products.forEach((product) {
      if (!result.containsKey(product.name)) {
        result[product.name] = {
          'amount': 1,
          'name': product.name,
          'price': product.price
        };
      } else {
        result[product.name]['amount']++;
      }
    });

    return result;
  }

  @override
  String toString() {
    return '$_products';
  }
}
