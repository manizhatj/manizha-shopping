import 'dart:io';
//import 'cart.dart';
import 'customer.dart';
import 'mall.dart';
import 'product.dart';
import 'store.dart';

void main() {
  // SETUP
  Mall mall = Mall();

  stdout.writeln('Please enter your name');
  String currentCustomerName = stdin.readLineSync();

  mall.selectCustomerByName(currentCustomerName);
  //Customer currentCustomer = mall.getCurrentCustomer;

  String storeChoice = "yes";
  while (storeChoice == "yes") {
    stdout.writeln('Pick your store: Gucci, Ikea, Chocolate Shop');
    String chosenStoreName = stdin.readLineSync();
    Store chosenStore = mall.selectStoreByName(chosenStoreName);
    print(
        'Here is a list of what you can buy at ${chosenStore.name}: ${chosenStore.products}');

    String response = "yes";

    while (response == "yes") {
      String selectedProductName = askToChooseProduct();

      while (!chosenStore.productPresent(selectedProductName)) {
        print(
            "We don't have a $selectedProductName you ff twat. Please select another product.");
        selectedProductName = askToChooseProduct();
      }
      int amount = getAmount();

      Product selectedProduct =
          chosenStore.getProductByName(selectedProductName);
      mall.addProductToCart(selectedProduct, amount);
      double totalPrice = mall.calculateTotalPrice();

      stdout.writeln("Keep shopping at ${chosenStore.name}? yes | no");
      response = stdin.readLineSync();
    }

    print(mall.receipt());

    stdout.writeln("Go to another store? yes | no");
    storeChoice = stdin.readLineSync();
  }
}

String askToChooseProduct() {
  stdout.writeln('Pick the product you want to buy');
  String selectedProductName = stdin.readLineSync();
  return selectedProductName;
}

int getAmount() {
  stdout.writeln('How many?');
  int amount = int.parse(stdin.readLineSync());
  return amount;
}
