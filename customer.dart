import 'cart.dart';
import 'product.dart';

class Customer {
  String name;
  double balance;
  Cart _cart;

  Customer(String name, double balance, Cart cart) {
    this.name = name;
    this.balance = balance;
    this._cart = cart;
  }

  void addProductToCart(Product product) {
    _cart.addProductToCart(product);
  }

  double calculateTotalPrice() {
    return _cart.calculateTotalPrice();
  }

  double updateBalance() {
    double newBalance;
    newBalance = this.balance - this.calculateTotalPrice();
    this.balance = newBalance;
    return newBalance;
  }

  String receipt() {
    String result = '';
    this._cart.getGroupedProducts().forEach((key, value) {
      result += value['amount'].toString() +
          'x ' +
          value['name'] +
          ': ' +
          value['price'].toString() +
          ' = ' +
          (value['price'] * value['amount']).toString() +
          '\n';
    });

    result += 'Total: ' + calculateTotalPrice().toString() + '\n';
    result += 'Your new balance: ' + updateBalance().toString();

    return result;
  }

  @override
  String toString() {
    return '$name: $balance, $_cart';
  }
}
